/* tuxedoWrapper.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
'use strict';

// Imports 
const GLib = imports.gi.GLib;

/**
 * Builds ans array of available colors, like ['0x000000', '0x000011', '0x000022', '0x000033', ..., '0xFFFFFF']
 * 
 * @returns: List of available colors
 */
function getAvailableColors() {
    const groups = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
    let output = [];
    for (first of groups) {
        for (second of groups) {
            for (third of groups) {
                output.push(`0x${first}${first}${second}${second}${third}${third}`)
            }
        }
    }
    return output;
}

// Constants
const AVAILABLE_COLORS = getAvailableColors();

const PREFIX = '/sys/devices/platform/tuxedo_keyboard';

const STATE = {
    name: 'state',
    file: PREFIX + "/state", 
    avalaibleValues: [0, 1], 
    mode: ["r", "w"]
};

const MODE = {
    name: 'mode',
    file: PREFIX + "/mode", 
    avalaibleValues: Array.from(Array(8).keys()), 
    mode: ["r", "w"]
};

const BRIGHTNESS_FILE = {
    name: 'brightness',
    file: PREFIX + "/brightness", 
    avalaibleValues: Array.from(Array(256).keys()), 
    mode: ["r", "w"]
};

const EXTRA = {
    name: 'extra',
    file: PREFIX + "/state", 
    avalaibleValues: [0, 1], 
    mode: ["r"]
};

const COLOR_EXTRA = {
    name: 'color_extra',
    file: PREFIX + "/color_extra", 
    avalaibleValues: AVAILABLE_COLORS, 
    mode: ["r", "w"]
};

const COLOR_LEFT = {
    name: 'color_left',
    file: PREFIX + "/color_left", 
    avalaibleValues: AVAILABLE_COLORS, 
    mode: ["r", "w"]
};

const COLOR_RIGHT = {
    name: 'color_right',
    file: PREFIX + "/color_right", 
    avalaibleValues: AVAILABLE_COLORS, 
    mode: ["r", "w"]
};

const COLOR_CENTER = {
    name: 'color_center',
    file: PREFIX + "/color_center", 
    avalaibleValues: AVAILABLE_COLORS, 
    mode: ["r", "w"]
};

/**
 * Thin wrapper arround the tuxedo-keyboard sysfs.
 * Reads / writes from/into the sysfs files, to perform operation on the keyboard lightning.
 * See https://github.com/tuxedocomputers/tuxedo-keyboard
 */
class TuxedoWrapper {
    constructor() {
        if (!(GLib.file_test(PREFIX, GLib.FileTest.EXISTS) || GLib.file_test(PREFIX, GLib.FileTest.IS_DIR) )) {
            const msg = `The tuxedo keyboard drivers is not installed: the sysfs directory '${PREFIX}' does not exists !`;
            log(msg);
            throw new Exception(msg);
        }
    }

    _getValue(target) {
        if (!(GLib.file_test(target.file, GLib.FileTest.EXISTS))) {
            const msg = `The tuxedo keyboard drivers is not installed: the sysfs file '${target.file}' does not exists !`;
            log(msg);
            throw new Exception(msg);
        }
        let res = GLib.file_get_contents(target.file)
        if (res[0]) {
            return res[1]
        } else {
            return null;
        }
    }

    _setValue(target, value) {
        // Checks the target file exists
        if (!(GLib.file_test(target.file, GLib.FileTest.EXISTS))) {
            const msg = `The tuxedo keyboard drivers is not installed: the sysfs file '${target.file}' does not exists !`;
            log(msg);
            throw new Exception(msg);
        }

        // Checks that the calue is acceptable
        if (!(target.avalaibleValues.indexOf(value) > 0)) {
            const msg = `The value ${value} is not acceptable for target ${target.name}`;
            log(msg);
            throw new Exception(msg);
        }

        // Actually writes the value 
        try {
            const fd = GLib.open(target.file, "w");
            fd.write(value);
        } catch (e) {
            const msg = `Unable to write into the sysfs file '${target.file}': ${e}`;
            log(msg);
            throw new Exception(msg);
        }
    }

    getState() {
        return this._getValue(STATE);
    }

    setState(value) {
        this._setValue(STATE, value);
    }

    getMode() {
        return this._getValue(MODE);
    }

    setMode(value) {
        this._setValue(MODE, value)
    }

    getColorLeft() {
        return this._getValue(COLOR_LEFT);
    }

    setColorLeft(value) {
        this._setValue(COLOR_LEFT, value)
    }

    getColorRight() {
        return this._getValue(COLOR_RIGHT);
    }

    setColorRight(value) {
        this._setValue(COLOR_RIGHT, value)
    }

    getColorCenter() {
        return this._getValue(COLOR_CENTER);
    }

    setColorCenter(value) {
        this._setValue(COLOR_CENTER, value)
    }

    getColorExtra() {
        return this._getValue(COLOR_EXTRA);
    }

    setColorExtra(value) {
        this._setValue(COLOR_EXTRA, value)
    }

    getExtra() {
        return this._getValue(EXTRA);
    }
}