# A Gnome extension to control Tuxedo keyboard backlight

## Tuxedo keyboard setup

The keyboard is bound with the driver from [https://github.com/tuxedocomputers/tuxedo-keyboard](https://github.com/tuxedocomputers/tuxedo-keyboard).

The driver provides a sysfs entry in:

```
 /sys/devices/platform/tuxedo_keyboard
```

## Systemd service file

The normal users are not allowed to write into the sysfs files. More, those files are created at boot time by the kernel module, so you cannot set perms at boot... 

The systemd service just changes the sysfs files group and adds a `g+r` perm.

You have to create a tuxedo group:

```sh
$ sudo addgroup tuxedo
$ sudo adduser ${USER} tuxedo
```

And activate the service:

```sh
$ cd systemd
$ cp tuxedo.service /etc/systemd/system/
$ sudo systemctl enable tuxedo.service
$ sudo systemctl start tuxedo.service
```
